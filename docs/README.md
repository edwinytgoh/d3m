# Contributing Documentation to the D3M Project

## Introduction
This project uses [Sphinx](https://www.sphinx-doc.org/en/master/) to generate documentation. They maintain an excellent
tutorial that gives an in-depth overview of how it works, but we'll provide basic instructions here to get you started.

## Getting Started
All documentation exists in this repo in reStructuredText `rst` files located in the `docs` directory. Basic workflow
for contributing to documentation will be to make your changes locally, use Sphinx to generate the `html` pages,
review the documentation website locally, then finally, committing to this repo once you're satisfied with your changes.

1. Install Sphinx and the Sphinx extensions used by D3M onto your local machine/virtual environment.
    ```
    pip install sphinx recommonmark sphinxcontrib-fulltoc sphinx-autodoc-typehints
    ```

1. Now, we'll use Sphinx to render the webpages. From the root directory of the repo, use Sphinx to take the contents in
`docs` as input and output html pages to a convenient location, say `/home/username/d3m_docs`:
    ```
    sphinx-build -b html docs ~/d3m_docs
    ```

1. Now you can open up the resulting HTML files in the directory you specified to view the rendered docs, `index.html` being the main page.

## Adding Content
If you're just updating existing docs, you can write your changes in the appropriate `rst` files, review your work
using the steps above, commit, and submit a merge request.

To add completely new documentation, you'll have a few
extra steps to link your `rst` file to the main docs.

1. Create `rst` file in `docs` directory.
1. Open up `docs/index.rst` where you'll see a file similar to below:

    ```
     D3M core package's documentation
    ================================

    :Version: |version|

    This is documentation for the common code for D3M project,
    the ``d3m`` core package.

    .. toctree::
    :maxdepth: 2

    installation
    quickstart
    tutorial
    interfaces
    discovery
    metadata
    primitives_base_classes
    pipeline
    primitive-checklist
    reference

    Miscellaneous pages
    -------------------

    * :ref:`about`
    * :ref:`repostructure`

    Indices and tables
    ------------------

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
    ```
3. Place a reference to the `rst` file you just created in the appropriate section in the `index.rst` file. Be sure to leave spaces as they appear in the file or else Sphinx might throw an error and the site will get mangled. At this point, you can render the pages using the instructions above and review changes before committing.

## Suggesting Doc Changes
To suggest changes, please open a [new issue](https://gitlab.com/datadrivendiscovery/d3m/-/issues/new?issue%5B) on the D3M GitLab repo that explains which parts of the documentation require correction/clarification. Examples on how you might go about addressing this issue will help other contributors make those changes.




