========================
Getting Started with D3M
========================
---------------------------------------------------------------------------
A quick introduction to automated machine learning and the D3M architecture
---------------------------------------------------------------------------
Introduction
============

.. image:: images/ml_process.png
  :width: 600px
  :align: center

Generating effective machine learning models is a complicated multi-step process that usually involves
the participation of experts in several areas such as subject matter experts, data scientists, data engineers, and
ML algorithm engineers to name a few. D3M is a framework created to simplify this process by
automatically generating pipelines that lead from raw data to accurate predictions. The ultimate goal of D3M is
to automate the portions of the machine learning deployment process traditionally carried out by data experts,
allowing domain experts who have deep knowledge of the subject at hand (scientific, engineering,
business, healthcare, etc.) to deploy effective predictive models.

.. image:: images/automl_process.png
  :width: 600px
  :align: center

Overview D3M Architecture
============================
D3M is composed of three major components which we'll briefly describe here.

TA1: Primitives
---------------
Primitives are the core building blocks of the D3M system. You can think of them as the vocabulary of a machine
learning pipeline, which could be building blocks for data preprocessing (normalizing, scaling, imputing missing
values, dealing with outliers, etc.), featurizing or feature selection (one-hot encoding, vectorizing, segmentation,
etc.) and ML/DL models like `Scikit-Learn's <https://sklearn.org/documentation.html>`_ `RandomForestRegressor
<https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html>`_. You can get
started with creating TA1 primitives using the :ref:`quickstart`